package com.softconsis.microservicio.app.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SoftconsisServicioGatewayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftconsisServicioGatewayServerApplication.class, args);
	}

}
